package stjepan.simple_calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.evFirstNum) EditText evFirstNum;
    @BindView (R.id.evSecondNum) EditText evSecondNum;
    @BindView(R.id.tvResult) TextView tvResult;
    @BindView (R.id.btnDijeli) Button btDijeli;
    @BindView (R.id.btnMnozi) Button btMnozi;
    @BindView (R.id.btnOduzmi) Button btOduzmi;
    @BindView (R.id.btnZbroji) Button btZbroji;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
}
   @OnClick({R.id.btnOduzmi, R.id.btnZbroji, R.id.btnDijeli, R.id.btnMnozi})
   public void calculate(View v){
       //empty result tv
       this.tvResult.setText("");
       int btnId = v.getId();

       String first = this.evFirstNum.getText().toString();
       String second = this.evSecondNum.getText().toString();
       Double firstNum = 0.0;
       Double secondNum = 0.0;
       Double result = 0.0;

       boolean inputIsInvalid = false;
       boolean divisionByZero = false;
       if (first.equals("") && second.equals("")){
           Toast.makeText(this, "Bad inputs!", Toast.LENGTH_SHORT).show();
       }
       else{
               if (first.equals("")){
                   //firstNum is already initialized as 0.0
                   Toast.makeText(this, "Enter first number!", Toast.LENGTH_SHORT).show();
                   inputIsInvalid = true;
               }
               else{
                   firstNum =  Double.valueOf(first);
               }
               if (second.equals("")){
                   //secondNum is already initialized as 0.0
                   Toast.makeText(this, "Enter second number!", Toast.LENGTH_SHORT).show();
                   inputIsInvalid = true;
               }
               else{
                   secondNum = Double.valueOf(this.evSecondNum.getText().toString());
               }
               if (!inputIsInvalid){
                   switch(btnId){
                       case R.id.btnOduzmi:
                           result = firstNum - secondNum;
                           break;
                       case R.id.btnZbroji:
                           result = firstNum + secondNum;
                           break;
                       case R.id.btnMnozi:
                           result = firstNum * secondNum;
                           break;
                       case R.id.btnDijeli:
                           if (secondNum == 0.0){
                               Toast.makeText(this, "ERROR: Division by zero!", Toast.LENGTH_SHORT).show();
                               divisionByZero = true;
                               break;
                           }
                           else{
                               result = firstNum / secondNum;
                               break;
                           }
                   }
                   if (!divisionByZero)
                   this.tvResult.setText(String.valueOf(result));
               }
       }
   }
}